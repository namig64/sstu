﻿using Parser.Logics;
using Parser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Client.Controllers
{
	[RoutePrefix("rest")]	// Префикс контроллера
    public class RestController : ApiController		// Web Api контроллер, для взаимодействия с клиентом
    {
		public static ClientLogic ClientLogic { get { return Logics.ClientLogic; } }	// Класс для взаимодействия с хранилищем данных
		
		// Метод для получения всех данных, хранящихся в Triple Store (факультеты, кафедры, направления, дисциплины)
		[Route("getData")]			// Url маршрут метода. Cсылка будет выглядет следующим образом http://localhost/rest/getData, где /rest/ - префикс контроллера
		public string GetData()
		{
			var json = ClientLogic.GetData();	// Получаем данные в формате json
			return json;						// Возвращаем клиенту
		}

		// Метод получения полного описания дисциплины по его id
		[Route("getDisciplineById/{id}")]			
		public string getDisciplineById(string id)
		{
			return ClientLogic.GetDisciplineById(id);
		}

		// Метод получения всех дисциплин по программе с заданным id
		[Route("getDisciplinesByProgramId/{id}")]
		public string GetDisciplinesByProgramId(string id)
		{
			return ClientLogic.GetDisciplinesByProgramId(id);
		}

    }
}
