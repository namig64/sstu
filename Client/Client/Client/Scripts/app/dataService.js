﻿(function () {

    // Объяляем сервис в модуле app, передаем в зависимость модуль $http для отправки запросов на сервер
	angular
		.module('app')
		.factory('dataService', ['$http', dataService]);    

    // Реализуем логику сервиса
	function dataService($http) {

	    var restUrl = "http://localhost:11547/rest/";   // Ссылка к webapi контроллеру

		return {
			getData: getData, 
			getDisciplineById: getDisciplineById,
			getDisciplinesByProgramId: getDisciplinesByProgramId
		};

	    // Метод для получения всех данных, хранящихся в Triple Store 
		function getData() {
		    // Отправляем запрос, полученные данные обрабатываем в контроллере
			return $http.get(restUrl + 'getData');  
		}

	    // Метод получения полного описания дисциплины по его id
		function getDisciplineById(id) {
			return $http.get(restUrl + 'getDisciplineById/' + id);
		}

	    // Метод получения всех дисциплин по программе с заданным id
		function getDisciplinesByProgramId(id) {
			return $http.get(restUrl + 'getDisciplinesByProgramId/' + id);
		}
	}

}());
