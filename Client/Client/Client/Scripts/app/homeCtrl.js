﻿(function () {
	'use strict';

    // Объявляем контроллер в модуле app
	angular
        .module('app')
        .controller('homeCtrl', ['$scope', 'dataService', homeCtrl]);

    // В зависимости передаем $scope - пространство для работы с переменными контроллера и их использования на странице
    // dataService - созданный сервис для работы с данными
	function homeCtrl($scope, dataService) {

        

	    $scope.sel = {};
	    $scope.loadingText = null;

	    $scope.submit = submit;
	    $scope.changeFaculty = changeFaculty;
	    $scope.changeDepartment = changeDepartment;
	    $scope.changeProgram = changeProgram;

	    // Вызываем метод инициализации
	    init();
	    function init() {
	        // Отправляем запрос на получения всех данных в Triple store, используя dataService
	        dataService.getData().then(function (response) {
	            // Ответ, полученный в формате json преобразуем в javascript объекты
	            // Результат помещаем в переменную $scope.faculties для использования на странице
	            $scope.faculties = angular.fromJson(response.data);
	        });
	    }

	    function changeFaculty() {
	        $scope.sel.department = null;
	        $scope.sel.program = null;
	        unselectDiscipline();
	    }

	    function changeDepartment() {
	        $scope.sel.program = null;
	    }

	    function unselectDiscipline() {
	        $scope.sel.discipline = null;
	        $scope.disFull = null;
	    }

	    function changeProgram() {
	        unselectDiscipline();
	        var program = $scope.sel.program;
	        if (program.disciplines.length === 0) {
	            $scope.loadingText = "Загружаем дисциплины по данному направлению...";
	            dataService.getDisciplinesByProgramId(program.id).then(function (response) {
	                program.disciplines = angular.fromJson(response.data);
	                $scope.loadingText = null;
	            });
	        }
	    }


	    function submit() {
	        var dis = $scope.sel.discipline;
	        if (dis) {
	            if (!dis.description) {
	                $scope.loadingText = "Загружаем полное описание дисциплины...";
	                dataService.getDisciplineById(dis.id).then(function (response) {
	                    $scope.disFull = angular.fromJson(response.data);
	                    $scope.loadingText = null;
	                });
	            }
	            else {
	                $scope.disFull = dis;
	            }
	        }
	    }

		init();
		function init() {
			dataService.getData().then(function (response) {
				$scope.faculties = angular.fromJson(response.data);
			});
		}
        
	}

})();