﻿using BrightstarDB;
using BrightstarDB.Client;
using BrightstarDB.EntityFramework;
using Parser.Models;
using Parser.OntologyContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Logics
{
	public class BrightstarLogic
	{
		public string ConnectionString { get; set; }            // Строка соединения для подключения StoreService и создания ModelsContext
		public string StoreName { get; set; }                   // Название store
		public string StoreDirectoryPath { get; set; }          // Папка, в которой будет создан Store
		public ModelsContext Ctx { get; set; }                  // Контекст для работы с сущностями
		public IBrightstarService StoreService { get; set; }    // Сервис для работы со store
		public string ImportedFileName { get; set; }            // Имя файла на вход (структура онтологии)
		public string ExportedFileName { get; set; }            // Имя результирущего файла

		// Инициализируем свойства
		public BrightstarLogic()
		{
			StoreName = "SSTU";
			StoreDirectoryPath = Logics.UtilsLogic.FilesDir + "Parser/Parser/Store";
			ConnectionString = String.Format("type=embedded;storesdirectory={0};optimisticLocking=true;storename={1}", StoreDirectoryPath, StoreName);
			ImportedFileName = "sstu_structure.rdf";
			ExportedFileName = "sstu_result.rdf";

			Initialize();
		}

		public void Initialize()
		{
			// Создаем Service, для работы со store
			StoreService = BrightstarService.GetClient(ConnectionString);
			if (StoreService.DoesStoreExist(StoreName))
			{
				//StoreService.DeleteStore(StoreName);
			}

			// Создаем контекст
			Ctx = new ModelsContext(ConnectionString, true);
		}

		// Импортируем структуру онтологии
		public void ClearList<T>(IEnumerable<T> list)
		{
			foreach (var item in list)
			{
				Ctx.DeleteObject(item);
			}
			Ctx.SaveChanges();
		}

		public void ClearStore()
		{
			StoreService.DeleteStore(StoreName);
			StoreService.CreateStore(StoreName);
			var importJob = StoreService.StartImport(StoreName, ImportedFileName);
			Ctx = new ModelsContext(ConnectionString);
		}

		public void ImportStructure(string filename = "")
		{
			if (string.IsNullOrWhiteSpace(filename))
			{
				filename = ImportedFileName;
			}
			var importJob = StoreService.StartImport(StoreName, filename);
			Ctx.SaveChanges();
		}

		// Экспортируем результирующюю онтологию в файл
		public void ExportOntology(string filename = "")
		{
			if (string.IsNullOrWhiteSpace(filename))
			{
				filename = ExportedFileName;
			}
			var exportGraphJob = StoreService.StartExport(StoreName, filename, null, RdfFormat.RdfXml);
		}

	}
}
