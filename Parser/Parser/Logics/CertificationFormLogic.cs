﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser.Models;

namespace Parser.Logics
{
	public class CertificationFormLogic
	{
		public CertificationForm Zachet { get; set; }
		public CertificationForm Ekzamen {get; set;}

		public CertificationFormLogic()
		{
			Zachet = new CertificationForm("Зачет");
			Ekzamen = new CertificationForm("Экзамен");
		}

	}
}
