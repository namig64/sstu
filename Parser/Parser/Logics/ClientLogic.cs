﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Parser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Logics
{

	public class ClientLogic
	{
		public ParserLogic ParserLogic { get { return Logics.ParserLogic; } }
		public JsonSerializerSettings jsonSettings = new JsonSerializerSettings
		{
			//PreserveReferencesHandling = PreserveReferencesHandling.Objects,
			ContractResolver = new CamelCasePropertyNamesContractResolver()
		};

		public ClientLogic()
		{
			ParserLogic.ReadOntologyFromTheStore();
		}

		public string GetDisciplinesByProgramId(string programId)
		{
			var program = ParserLogic.Programs.FirstOrDefault(m => m.Id == programId);
			ParserLogic.SeleniumLogic.Init();
			ParserLogic.ParseDisciplines(program);

			var json = JsonConvert.SerializeObject(program.Disciplines, jsonSettings);
			return json;
		}

		public string GetDisciplineById(string id)
		{
			var dis = ParserLogic.Disciplines.FirstOrDefault(m => m.Id == id);
			ParserLogic.SeleniumLogic.Init();
			ParserLogic.ParseDisciplineInDeep(dis);
			var json = JsonConvert.SerializeObject(dis, jsonSettings);
			return json;
		}

		public string GetData()
		{
			var json = JsonConvert.SerializeObject(ParserLogic.Faculties, jsonSettings);
			return json;
		}
	}


}
