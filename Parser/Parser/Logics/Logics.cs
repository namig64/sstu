﻿using Parser.Models;
using Parser.OntologyContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Logics
{
	public static class Logics
	{
		public static ParserLogic ParserLogic { get; set; }
		public static ClientLogic ClientLogic { get; set; }
		public static BrightstarLogic BrightstarLogic { get; set; }
		public static ModelsContext Ctx { get { return BrightstarLogic.Ctx; } }
		public static SeleniumLogic SeleniumLogic { get; set; }
		public static CertificationFormLogic CertificationFormLogic { get; set; }
		public static PartDisLogic PartDisLogic { get; set; }
		public static ScientificWorkLogic ScientificWorkLogic { get; set;}
		public static UtilsLogic UtilsLogic { get; set;}

		public static void Init()
		{
			// Раставляем в порядке зависимостей
			UtilsLogic = new UtilsLogic();
			BrightstarLogic = new BrightstarLogic();
			CertificationFormLogic = new CertificationFormLogic();
			PartDisLogic = new PartDisLogic();
			ScientificWorkLogic = new ScientificWorkLogic();
			SeleniumLogic = new SeleniumLogic();

			ParserLogic = new ParserLogic();
			ClientLogic = new ClientLogic();
		}

	}
}
