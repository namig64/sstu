﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser.Models;
using BrightstarDB.EntityFramework;
using BrightstarDB.Client;
using Parser.OntologyContext;
//using Program = Parser.Models.Program;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Threading;

namespace Parser.Logics
{
	public class ParserLogic
	{
		#region Variables

		// Store
		public BrightstarLogic BrightstarLogic { get { return Logics.BrightstarLogic; } }
		public ModelsContext Ctx { get { return Logics.Ctx; } }

		// Selenium 
		public SeleniumLogic SeleniumLogic { get { return Logics.SeleniumLogic; } }
		public IWebDriver Driver { get { return SeleniumLogic.Driver; } }

		// Ontology Classes
		public CertificationFormLogic CertForms { get { return Logics.CertificationFormLogic; } }
		public PartDisLogic PartDisLogic { get { return Logics.PartDisLogic; } }
		public ScientificWorkLogic SWs { get { return Logics.ScientificWorkLogic; } }

		// Ontology lists
		public List<Faculty> Faculties { get; set; }
		public List<Department> Departments { get; set; }
		public List<Program> Programs { get; set; }
		public List<Discipline> Disciplines { get; set; }

		// UtilityLogic
		public UtilsLogic Utils { get { return Logics.UtilsLogic; } }

		public ParserLogic()
		{
			Faculties = new List<Faculty>();
			Departments = new List<Department>();
			Programs = new List<Program>();
			Disciplines = new List<Discipline>();
		}

		#endregion

		public void StartParse()
		{
			//BrightstarLogic.ImportStructure("sstu_disciplineInDeep_pin_ping_BD.rdf");
			ReadOntologyFromTheStore();
			SeleniumLogic.Init();

			ParseProgramsByDepartments();

			//var program = Programs.FirstOrDefault(m => m.NameFull == "Программная инженерия (бакалавры)");
			//ParseDisciplines(program);
			//var bd = Disciplines.FirstOrDefault(m=>m.NameFull == "Базы данных");
			//ParseDisciplineInDeep(bd);

			SaveAndClose("sstu_disciplines_ping_bd.rdf");
		}

		#region ParseMethods

		public void ParseFaculties(bool parseDepartments = false)
		{
			Driver.Url = SeleniumLogic.Url + "/Fakult";
			IEnumerable<IWebElement> elems = Driver.FindElements(By.CssSelector(".ms-summarycustombody .ms-vb a"));

			Regex regexFacultCode = new Regex("/Fakult/(.*)/Pages");
			foreach (IWebElement elem in elems)
			{
				try
				{
					var href = elem.GetAttribute("href");
					Match match = regexFacultCode.Match(href);
					string nameShort = match.Groups[1].ToString();

					if (nameShort == "VK")
					{
						continue;
					}

					var faculty = new Faculty();
					faculty.NameFull = elem.Text;
					faculty.Href = href;
					faculty.NameShort = nameShort;
					faculty.Id = GetId(nameShort);
					Faculties.Add(faculty);
				}
				catch (Exception ex)
				{

				}
			}

			if (parseDepartments == true)
			{
				foreach (Faculty item in Faculties)
				{
					ParseDepartments(item);
				}
			}


		}

		#region Departments

		public void ParseDepartments(Faculty faculty, bool parsePrograms = false)
		{
			Driver.Url = faculty.Href;
			IEnumerable<IWebElement> departments = Driver.FindElements(By.CssSelector("#zz2_QuickLaunchMenun0 + tr a"));
			foreach (IWebElement elem in departments)
			{
				string nameFull = elem.Text;
				string href = elem.GetAttribute("href");
				Regex regex = new Regex("\\((?<code>.*)\\)");
				Match match = regex.Match(nameFull);
				string nameShort = match.Groups["code"].ToString();


				Department dep = new Department();
				if (string.IsNullOrWhiteSpace(nameShort))
				{
					continue;
				}

				dep.Id = GetId(nameShort);
				dep.NameShort = nameShort;
				dep.NameFull = nameFull;
				dep.Faculty = faculty;
				dep.Href = href;

				Departments.Add(dep);
			}

			if (parsePrograms)
			{
				foreach (Department item in Departments)
				{
					ParseProgramsFromPortal(item);
				}
			}

		}

		public void ParseDepartmentsByFaculties()
		{
			foreach (var item in Faculties)
			{
				ParseDepartments(item);
			}
		}

		#endregion Departments

		#region Programs

		public void ParsePrograms(Department department)
		{
			ParseProgramsFromPortal(department);
			ParseProgramsFromUmu();
			ParseProgramsFromSstu();
		}

		public void ParseProgramsByDepartments()
		{
			foreach (var item in Departments)
			{
				ParseProgramsFromPortal(item);
			}
			ParseProgramsFromUmu();
			ParseProgramsFromSstu();
		}

		public void ParseProgramsFromPortal(Department department, bool parseDisciplines = false)
		{
			Driver.Url = department.Href;
			//IEnumerable<IWebElement> elems = Driver.FindElements(By.CssSelector("a.zz2_QuickLaunchMenu_3[href*=AllItems]"));

			IEnumerable<IWebElement> elems = Driver.FindElements(By.CssSelector(".ms-quickLaunch a[href*='Lists/List']"));
			foreach (IWebElement elem in elems)
			{
				if (elem.GetAttribute("href").IndexOf("List/AllItems") != -1)
				{
					continue;
				}
				var program = new Models.Program();
				program.Department = department;
				program.NameFull = elem.Text;
				program.Href = elem.GetAttribute("href");
				program.Id = GetId(program.NameFull);

				Programs.Add(program);
			}

			if (parseDisciplines)
			{
				foreach (var item in Programs)
				{
					ParseDisciplines(item);
				}
			}
		}

		public void ParseProgramsFromUmu()
		{
			Driver.Url = "http://umu.sstu.ru/dviz/standarts.html";
			//IEnumerable<IWebElement> elems = Driver.FindElements(By.CssSelector("table")).First().FindElements(By.CssSelector("tr"));
			IEnumerable<IWebElement> elems = Driver.FindElements(By.CssSelector("table tr"));

			foreach (var elem in elems)
			{
				try
				{
					var hrefStandart = elem.FindElement(By.CssSelector("td:nth-child(3) a"));
					if (hrefStandart != null)
					{
						string elemCode = elem.FindElement(By.CssSelector("td:nth-child(1)")).Text;
						string elemName = elem.FindElement(By.CssSelector("td:nth-child(2)")).Text;
						string elemStandart = hrefStandart.GetAttribute("href");
						string programTypeCode = elemCode.Substring(3, 2);

						foreach (var programType in Logics.UtilsLogic.ProgramTypes)
						{
							if (programTypeCode == programType.Code)
							{
								var program = Programs.FirstOrDefault(m => m.NameFull.Contains(elemName) && m.NameFull.Contains(programType.Name));
								if (program == null)
								{
									program = Programs.FirstOrDefault(m => m.NameFull.Contains(elemName));
								}
								if (program != null)
								{
									program.Code = elemCode;
									program.Standart = elemStandart;
								}
							}
						}
					}
				}
				catch (Exception ex)
				{
					// This is not Program element
				}

			}
		}

		public void ParseProgramsFromSstu()
		{
			Driver.Url = "http://www.sstu.ru/sveden/education/docs/";
			IEnumerable<IWebElement> elems = Driver.FindElements(By.CssSelector("#plans tr"));
			foreach (var elem in elems)
			{
				try
				{
					string elemCode = elem.FindElement(By.CssSelector("[itemprop='EduCode']")).Text;
					if (!string.IsNullOrWhiteSpace(elemCode))
					{
						string elemName = elem.FindElement(By.CssSelector("[itemprop='OOP_main']")).Text;

						Regex regexNameShort = new Regex("\\((.*)\\)");
						Match matchNameShort = regexNameShort.Match(elemName);
						string elemNameShort = matchNameShort.Groups[1].ToString();

						Regex regexNameFull = new Regex("^(.*)\\(");
						Match matchNameFull = regexNameFull.Match(elemName);
						string elemNameFull = matchNameFull.Groups[1].ToString().Trim();

						string elemHrefMetDocs = elem.FindElement(By.CssSelector("[itemprop='methodology'] a")).GetAttribute("href").ToString();

						// Если есть полное совпадение по коду и названию
						var program = Programs.FirstOrDefault(m => m.NameFull.Contains(elemNameFull) && m.Code == elemCode);

						// Если код неизвестен
						if (program == null)
						{
							program = Programs.FirstOrDefault(m => m.NameFull.Contains(elemNameFull));
						}

						if (program != null)
						{
							program.NameShort = elemNameShort;
							program.HrefMetDocs = elemHrefMetDocs;
						}

					}

				}
				catch (Exception ex)
				{

				}
			}
		}

		#endregion

		#region Disciplines

		public void ParseDisciplines(Program program)
		{
			ParseDisciplinesFromPortal(program);
			Save();
			ReadOntologyFromTheStore();
			ParseDisciplinesFromSstu(program);
		}

		public void ParseDisciplinesFromPortal(Program program)
		{
			Driver.Url = program.Href;
			IEnumerable<IWebElement> elems = Driver.FindElements(By.CssSelector("[id*=tbod] > tr"));
			foreach (IWebElement elem in elems)
			{
				try
				{
					Discipline dis = new Discipline();

					string elemCode = elem.FindElement(By.CssSelector("td:nth-child(2)")).Text;

					if (!string.IsNullOrEmpty(elemCode))
					{
						dis.Program = program;
						dis.Code = elemCode;

						IWebElement elemName = elem.FindElement(By.CssSelector("td:nth-child(3) a"));
						dis.NameFull = elemName.Text;
						dis.Id = GetId(dis.Program.NameShort + "__" + dis.NameFull);
						dis.Href = elemName.GetAttribute("href");

						// Определяем зачет или экзамен
						string semester = elem.FindElement(By.CssSelector("td:nth-child(4)")).Text;

						if (!string.IsNullOrWhiteSpace(semester))
						{
							dis.Semester = int.Parse(semester);
							dis.CertificationForm = CertForms.Ekzamen;
						}
						else
						{
							dis.Semester = int.Parse(elem.FindElement(By.CssSelector("td:nth-child(5)")).Text);
							dis.CertificationForm = CertForms.Zachet;
						}

						dis.Course = dis.Semester % 2 + dis.Semester / 2;

						#region ScientificWork
						string kp = elem.FindElement(By.CssSelector("td:nth-child(6)")).Text;
						string kr = elem.FindElement(By.CssSelector("td:nth-child(7)")).Text;
						string rgr = elem.FindElement(By.CssSelector("td:nth-child(8)")).Text;

						if (!string.IsNullOrWhiteSpace(kp))
						{
							dis.ScientificWorks.Add(SWs.KP);
						}

						if (!string.IsNullOrWhiteSpace(kr))
						{
							dis.ScientificWorks.Add(SWs.KR);
						}

						if (!string.IsNullOrWhiteSpace(rgr))
						{
							dis.ScientificWorks.Add(SWs.RGR);
						}
						#endregion

						string partDisCode = dis.Code.Substring(4, 1);
						foreach (var item in PartDisLogic.PartDises)
						{
							if (partDisCode == item.Code)
							{
								dis.PartDis = item;
								break;
							}
						}

						Disciplines.Add(dis);
					}

				}
				catch (Exception ex) { }

			}
		}

		public void ParseDisciplineInDeep(Discipline dis)
		{
			try
			{
				Driver.Url = dis.Href;
				dis.Description = Driver.FindElement(By.CssSelector("table[summary='Описание курса'] .ms-vb2")).Text;

				var elems = GetElemsFromEduMatBlock();
				string elemName;
				foreach (var elem in elems)
				{
					elemName = elem.Text;
					if (elemName.Contains("1"))
					{
						dis.HrefEduMat = elem.GetAttribute("href");
					}
					else if (elemName.Contains("2"))
					{
						dis.HrefEduMetMat = elem.GetAttribute("href");
					}
				}

				Driver.Url = dis.HrefEduMat;
				elems = GetElemsFromEduMatBlock();
				foreach (var elem in elems)
				{
					elemName = elem.Text;
					if (elemName.Contains("1.1"))
					{
						dis.HrefEduMatLectures = elem.GetAttribute("href");
					}
					else if (elemName.Contains("1.2"))
					{
						dis.HrefEduMatPresentations = elem.GetAttribute("href");
					}
				}

				Driver.Url = dis.HrefEduMatLectures;

				Driver.Url = dis.HrefEduMatLectures;
				elems = GetElemsFromEduMatBlock();
				dis.NumberOfLectures = elems.Count();

				Driver.Url = dis.HrefEduMatPresentations;
				elems = GetElemsFromEduMatBlock();
				dis.NumberOfPresentations = elems.Count();

				Driver.Url = dis.HrefEduMetMat;
				elems = GetElemsFromEduMatBlock();
				foreach (var elem in elems)
				{
					elemName = elem.Text;
					if (elemName.Contains("2.1"))
					{
						dis.HrefEduMetMatPW = elem.GetAttribute("href");
					}
					else if (elemName.Contains("2.4"))
					{
						dis.HrefEduMetMatSRS = elem.GetAttribute("href");
					}
				}

				Driver.Url = dis.HrefEduMetMatPW;
				elems = GetElemsFromEduMatBlock();
				dis.MetMatPW = elems.Count();

				Driver.Url = dis.HrefEduMetMatSRS;
				elems = GetElemsFromEduMatBlock();
				dis.MetMatSRS = elems.Count();
			}
			catch (Exception ex)
			{

			}
		}

		public void ParseDisciplinesFromSstu(Program program)
		{
			if (string.IsNullOrWhiteSpace(program.HrefMetDocs))
			{
				return;
			}
			Driver.Url = program.HrefMetDocs;
			IEnumerable<IWebElement> elems = Driver.FindElements(By.CssSelector(".bcenter tbody tr"));
			int count = 0;
			foreach (var elem in elems)
			{
				try
				{
					string elemCode = elem.FindElement(By.CssSelector("td:nth-child(1)")).Text;
					if (!string.IsNullOrWhiteSpace(elemCode))
					{
						string elemHrefWorkProgram = elem.FindElement(By.CssSelector("td:nth-child(5) a")).GetAttribute("href");

						string elemNameFull = elem.FindElement(By.CssSelector("td:nth-child(2)")).Text;

						// Если есть полное совпадение по коду и названию
						var dis = program.Disciplines.FirstOrDefault(m => m.NameFull.Contains(elemNameFull));

						if (dis == null)
						{
							dis = program.Disciplines.FirstOrDefault(m => m.Code == elemCode);
						}

						if (dis != null)
						{
							dis.WorkProgram = elemHrefWorkProgram;
							count++;
						}

					}

				}
				catch (Exception ex)
				{

				}
			}
		}

		public IEnumerable<IWebElement> GetElemsFromEduMatBlock()
		{
			var elems = Driver.FindElements(By.CssSelector("#onetidDoclibViewTbl0 tr td.ms-vb-title a[href]"));
			return elems;
		}

		#endregion

		#endregion

		#region Test

		public void TestDepartments()
		{
			ParseFaculties(parseDepartments: false);
			ParseDepartments((Faculty)Ctx.Faculties.FirstOrDefault());
		}

		public void TestPrograms()
		{
			ParseProgramsFromPortal(new Department() { Href = "https://portal.sstu.ru/Fakult/FTF/MIM/default.aspx" });
			ParseProgramsFromUmu();
			ParseProgramsFromSstu();
		}

		#endregion

		#region Other Methods

		public void ReadOntologyFromTheStore()
		{
			Faculties.Clear();
			Departments.Clear();
			Programs.Clear();
			Disciplines.Clear();

			foreach (var item in Ctx.Faculties.ToList())
			{
				Faculties.Add((Faculty)item);
			}

			foreach (var item in Ctx.Departments.ToList())
			{
				Departments.Add((Department)item);
			}

			foreach (var item in Ctx.Programs.ToList())
			{
				Programs.Add((Program)item);
			}

			foreach (var item in Ctx.Disciplines.ToList())
			{
				Disciplines.Add((Discipline)item);
			}
		}

		public void SaveAndClose(string outputFile = "sstu_result.rdf")
		{
			Save();
			BrightstarLogic.ExportOntology(outputFile);
			SeleniumLogic.Driver.Quit();
		}

		public void Save()
		{
			Ctx.AddRange(Faculties);
			Ctx.AddRange(Departments);
			Ctx.AddRange(Programs);
			Ctx.AddRange(Disciplines);
			Ctx.SaveChanges();
		}

		public string GetId(string name)
		{
			return name.Replace(' ', '_').Trim();
		}

		#endregion

	}

}
