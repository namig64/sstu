﻿using Parser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Logics
{
	public class PartDisLogic
	{
		public List<PartDis> PartDises { get; set; } 

		public PartDisLogic()
		{
			PartDises = new List<PartDis>{
				new PartDis("Базовая", "1"),
				new PartDis("Вариативная", "2"),
				new PartDis("ПоВыбору", "3")
			};

		}

	}
}
