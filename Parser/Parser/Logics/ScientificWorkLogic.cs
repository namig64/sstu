﻿using Parser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Logics
{
	public class ScientificWorkLogic
	{
		public ScientificWork KP { get; set; }
		public ScientificWork KR { get; set; }
		public ScientificWork RGR { get; set; }

		public ScientificWorkLogic()
		{
			KP = new ScientificWork("КП", "5");
			KR = new ScientificWork("КР", "6");
			RGR = new ScientificWork("РГР", "7");
		}

	}
}
