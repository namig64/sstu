﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Parser.Logics
{
	public class SeleniumLogic
	{
		public string Username { get; set; }
		public string Password { get; set; }
		public string ChromeDriverPath { get; set; }
		public IWebDriver Driver { get; set; }

		public string BaseUrl { get; set; }
		public string Url { get; set; }

		public SeleniumLogic()
		{
			Username = "sstuedudom\\130841";
			Password = "1662716";
			BaseUrl = "http://portal.sstu.ru";
			Url = GetFullUrl(BaseUrl);
			ChromeDriverPath = Logics.UtilsLogic.FilesDir + "Files";
		}

		public void Init()
		{
			if (Driver == null)
			{
				Driver = new ChromeDriver(ChromeDriverPath);
				Driver.Url = Url;
			}
		}

		public string GetFullUrl(string url)
		{
			Regex regex = new Regex("//(.*)");
			Match match = regex.Match(url);
			string domain = match.Groups[1].ToString();

			string encodedUsername = HttpUtility.UrlEncode(Username);
			string encodedPassword = HttpUtility.UrlEncode(Password);

			string resultUrl = String.Format("https://{0}:{1}@{2}", encodedUsername, encodedPassword, domain);
			return resultUrl;
		}

	}
}
