﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Parser.Logics
{

	public class UtilsLogic
	{
		public List<ProgramType> ProgramTypes { get; set; }
		public string FilesDir { get; set; }

		public UtilsLogic()
		{
			ProgramTypes = new List<ProgramType>{
                new ProgramType("03", "бакалавры"),
                new ProgramType("04", "магистры"),
                new ProgramType("05", "специалисты")};

			//"C:\\Users\\namig\\Desktop\\SSTU\\Parser\\Parser\\bin\\Debug\\"

			var rundir = AppDomain.CurrentDomain.BaseDirectory;
			var index = rundir.IndexOf("SSTU\\");
			FilesDir = rundir.Substring(0, index + 5);

		}

	}

	public class ProgramType
	{
		public string Code { get; set; }
		public string Name { get; set; }

		public ProgramType(string code, string name)
		{
			Code = code;
			Name = name;
		}

	}

}
