﻿using Parser.Logics;
using Parser.OntologyContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Models
{
	public partial class CertificationForm  {

		public static ModelsContext Ctx { get { return Logics.Logics.Ctx; } }

		public CertificationForm(string name)
		{
			Id = name;
			Context = Ctx;

			Ctx.SaveChanges();
		}

	}
}
