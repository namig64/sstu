﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser.OntologyContext;

namespace Parser.Models
{
	public partial class PartDis
	{
		public static ModelsContext Ctx { get { return Logics.Logics.Ctx; } }

		public PartDis(string name, string code)
		{
			Id = name;
			Code = code;
			Context = Ctx;

			Ctx.SaveChanges();
		}

	}
}
