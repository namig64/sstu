﻿using BrightstarDB.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Models
{
	[JsonObject(MemberSerialization.OptIn)]
	[Entity("#CertificationForm")]
	public interface ICertificationForm
	{
		[JsonProperty]
		[Identifier("#")]
		string Id { get; }
	}

}
