﻿using BrightstarDB.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Models
{
	[JsonObject(MemberSerialization.OptIn)]
	[Entity("#Department")]
	public interface IDepartment
	{
		[JsonProperty]
		[Identifier("#")]
		string Id { get; }

		//[JsonProperty]
		[PropertyType("#locatedOn")]
		IFaculty Faculty { get; set; }

		[JsonProperty]
		[PropertyType("#hasNameShort")]
		string NameShort { get; set; }

		[JsonProperty]
		[PropertyType("#hasNameFull")]
		string NameFull { get; set; }

		[JsonProperty]
		[PropertyType("#href")]
		string Href { get; set; }

		[JsonProperty]
		[InverseProperty("Department")]
		ICollection<IProgram> Programs { get; set; }
	}
}
