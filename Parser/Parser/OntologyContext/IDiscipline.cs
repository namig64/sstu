﻿using BrightstarDB.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Models
{
	[JsonObject(MemberSerialization.OptIn)]
	[Entity("#Discipline")]
	public interface IDiscipline
	{
		[JsonProperty]
		[Identifier("#")]
		string Id { get; }

		// Object properties
		[PropertyType("#studiedOn")]
		IProgram Program { get; set; }

		[PropertyType("#href")]
		string Href { get; set; }

		[JsonProperty]
		[PropertyType("#hasCertificationForm")]
		ICertificationForm CertificationForm { get; set;}

		[JsonProperty]
		[PropertyType("#isPart")]
		IPartDis PartDis { get; set; }

		[JsonProperty]
		[PropertyType("#hasScientificWork")]
		ICollection<IScientificWork> ScientificWorks { get; set; }

		// Data type properties

		[PropertyType("#hasNameShort")]
		string NameShort { get; set; }

		[JsonProperty]
		[PropertyType("#hasNameFull")]
		string NameFull { get; set; }

		[JsonProperty]
		[PropertyType("#hasMetMatSRS")]
		int MetMatSRS { get; set;}

		[JsonProperty]
		[PropertyType("#hasMetMatPW")]
		int MetMatPW { get; set; }

		[JsonProperty]
		[PropertyType("#course")]
		int Course { get; set; }

		[JsonProperty]
		[PropertyType("#semester")]
		int Semester { get; set; }

		[JsonProperty]
		[PropertyType("#numberOfPresentations")]
		int NumberOfPresentations { get; set; }

		[JsonProperty]
		[PropertyType("#numberOfLectures")]
		int NumberOfLectures { get; set; }

		[JsonProperty]
		[PropertyType("#hasCode")]
		string Code { get; set; }

		[JsonProperty]
		[PropertyType("#hasWorkProgram")]
		string WorkProgram { get; set; }

		[JsonProperty]
		[PropertyType("#description")]
		string Description { get; set; }

		[PropertyType("#hrefEduMat")]
		string HrefEduMat {get; set;}

		[PropertyType("#hrefEduMatLectures")]
		string HrefEduMatLectures {get; set;}

		[PropertyType("#hrefEduMatPresentations")]
		string HrefEduMatPresentations {get; set;}

		[PropertyType("#hrefEduMetMat")]
		string HrefEduMetMat {get; set;}

		[PropertyType("#hrefEduMetMatSRS")]
		string HrefEduMetMatSRS {get; set;}

		[PropertyType("#hrefEduMetMatPW")]
		string HrefEduMetMatPW {get; set;}

	}
}
