﻿using BrightstarDB.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Models
{
	[JsonObject(MemberSerialization.OptIn)]
    [Entity("#Faculty")]
    public interface IFaculty
    {
		[JsonProperty]
		[Identifier("#")]
		string Id { get; }

		[JsonProperty]
		[PropertyType("#hasNameShort")]
        string NameShort { get; set; }

		[JsonProperty]
        [PropertyType("#hasNameFull")]
        string NameFull { get; set; }

		[JsonProperty]
		[InverseProperty("Faculty")]
		ICollection<IDepartment> Departments { get; set; }

		[JsonProperty]
		[PropertyType("#href")]
		string Href { get; set; }

    }
}
