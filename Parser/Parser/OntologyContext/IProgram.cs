﻿using BrightstarDB.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Models
{
	[JsonObject(MemberSerialization.OptIn)]
	[Entity("#Program")]
	public interface IProgram
	{
		[JsonProperty]
		[Identifier("#")]
		string Id { get; }

		// Object properties

		//[JsonProperty]
		[PropertyType("#provided")]
		IDepartment Department { get; set; }

		[JsonProperty]
		[InverseProperty("Program")]
		ICollection<IDiscipline> Disciplines { get; set; }

		// Datatype properties

		[JsonProperty]
		[PropertyType("#hasNameShort")]
		string NameShort { get; set; }

		[JsonProperty]
		[PropertyType("#hasNameFull")]
		string NameFull { get; set; }

		[JsonProperty]
		[PropertyType("#standart")]
		string Standart { get; set; }

		[JsonProperty]
		[PropertyType("#hasCode")]
		string Code { get; set; }

		[JsonProperty]
		[PropertyType("#href")]
		string Href { get; set; }

		[JsonProperty]
		[PropertyType("#hrefMetDocs")]
		string HrefMetDocs { get; set; }

	}
}
