﻿using BrightstarDB;
using BrightstarDB.Client;
using Parser.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser.Logics;

namespace Parser
{
    class Run
    {
		static void Main(string[] args)
		{
			Logics.Logics.Init();
			Logics.Logics.ParserLogic.StartParse();
			Console.WriteLine("Press key");
			Console.ReadKey();
		}
    }
}
